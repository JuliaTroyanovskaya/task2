package testprojects.automation.test2.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

/**
 * Created by user on 17.04.2017.
 */
public class EventHandler implements WebDriverEventListener {
    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        System.out.println("Navigate to " + s );
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        System.out.println("The page is " + webDriver.getTitle()+ " " + s);
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {

    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {

    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        System.out.println("Navigate forward " + webDriver.getTitle());
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        System.out.println("The page is " + webDriver.getTitle());
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {

    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {

    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Should be " + by);
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Element found " + by);
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        System.out.println("Should click " + webElement.getTagName());

    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        System.out.println("Clicked successfull");

    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        System.out.println("Start changing " + webElement.getTagName());
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        System.out.println("Changed " + webElement.getTagName());
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        System.out.println("Start Scrolling " + webDriver.getCurrentUrl()+ " " + webDriver.getTitle());
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        System.out.println("Scrolled " + webDriver.getTitle());
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {

    }
}
