package testprojects.automation.test2.tests;

import testprojects.automation.test2.BaseScript;
import org.openqa.selenium.WebDriver;
import testprojects.automation.test2.GeneralActions;

import java.util.Locale;

public class CreateCategoryTest extends BaseScript {

    private static final String LOGIN = "webinar.test@gmail.com";
    private static final String PASSWORD = "Xcg7299bnSmMuRLp9ITw";

    private static String myCategory = "MyLovelyCategory";


    public static void main(String[] args) throws InterruptedException {

        new Locale("UTF-8");

        WebDriver driver = getConfiguredDriver();

        GeneralActions actions = new GeneralActions(driver);

        // login
        actions.login(LOGIN, PASSWORD);
        // create myCategory
        actions.createCategory(myCategory);
        // check that new myCategory appears in Categories table
        actions.checkCategoryInTable(myCategory);
        // finish script
        driver.quit();
    }
}
