package testprojects.automation.test2;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import testprojects.automation.test2.utils.Properties;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    private By catalogLink = By.cssSelector("#subtab-AdminCatalog");
    private By categoryLink = By.cssSelector("#subtab-AdminCategories");
    private By addCategoryLink = By.cssSelector("#page-header-desc-category-new_category");
    private By newCategoryField = By.cssSelector("#name_1");
    private By saveCategoryButton = By.cssSelector("#category_form_submit_btn");
    private By confirmMessage = By.xpath("//div[contains(@class, 'alert alert-success')] //button");
    private By sortDownButton = By.xpath("//span[contains(text(), 'Имя')]//../a[2]");
    private By myCategory ;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {

        driver.navigate().to(Properties.getBaseAdminUrl());
        WebElement elLogin = driver.findElement(By.cssSelector("#email"));
        elLogin.sendKeys(login);

        WebElement elPassword = driver.findElement(By.cssSelector("#passwd"));
        elPassword.sendKeys(password);

        WebElement elSubmit = driver.findElement(By.cssSelector("[name = 'submitLogin']"));
        elSubmit.click();

        //System.out.println("Page title is: " + driver.getTitle());
    }

    /**
     * Adds new category in Admin Panel.
     * @param categoryName
     */
    public void createCategory(String categoryName) {

        Actions actions = new Actions(driver);

        waitForContentLoad(catalogLink);
        System.out.println("Page title is: " + driver.getTitle());
        WebElement catalogLink = driver.findElement(this.catalogLink);
        actions.moveToElement(catalogLink).build().perform();
        waitForContentLoad(categoryLink);
        WebElement categoryLink = driver.findElement(this.categoryLink);
        actions.click(categoryLink).build().perform();

        waitForContentLoad(addCategoryLink);
        System.out.println("Page title is: " + driver.getTitle());
        WebElement addCategoryLink = driver.findElement(this.addCategoryLink);
        actions.click(addCategoryLink).build().perform();

        waitForContentLoad(newCategoryField);
        WebElement newCategoryField = driver.findElement(this.newCategoryField);
        actions.sendKeys(newCategoryField, categoryName).build().perform();

        scrollPageDown();
        waitForContentLoad(saveCategoryButton);
        WebElement saveCategoryButton = driver.findElement(this.saveCategoryButton);
        actions.click(saveCategoryButton).build().perform();

        waitForContentLoad(confirmMessage);
        System.out.println("Page title is: " + driver.getTitle());
        WebElement confirmMessage = driver.findElement(this.confirmMessage);
        actions.moveToElement(confirmMessage).build().perform();

        WebElement sortDownButton = driver.findElement(this.sortDownButton);
        actions.click(sortDownButton).build().perform();
    }

    public void checkCategoryInTable(String categoryName) {
        String categoryXPath = "//td[contains(text(), '" + categoryName + "')]";
        myCategory = By.xpath(categoryXPath);
        waitForContentLoad(myCategory);
        WebElement myCategory = driver.findElement(this.myCategory);
        Actions actions = new Actions(driver);
        actions.click(myCategory).build().perform();
    }

    public void scrollPageDown() {

        JavascriptExecutor executor = (JavascriptExecutor)driver;
        boolean scrollResult = (boolean)executor.executeScript(
                "var scrollBefore = $(window).scrollTop;" +
                        "window.scrollTo(scrollBefore, document.body.scrollHeight);" +
                        "return $(window).scrollTop() > scrollBefore;");
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad(By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

}
